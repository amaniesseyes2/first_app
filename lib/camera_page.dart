import 'dart:async';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:first_app/preview_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

class CameraPage extends StatefulWidget {
const CameraPage({Key? key, required this.cameras}) : super(key: key);

final List<CameraDescription> cameras;

@override
State<CameraPage> createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
late CameraController _cameraController;
bool _isRearCameraSelected = true;

@override
void dispose() {
_cameraController.dispose();
super.dispose();
}

@override
void initState() {
super.initState();
initCamera(widget.cameras[0]);
}

void takePicture() async {
if (!_cameraController.value.isInitialized) {
return null;
}
if (_cameraController.value.isTakingPicture) {
return null;
}
try {
await _cameraController.setFlashMode(FlashMode.off);
XFile picture = await _cameraController.takePicture();
Uint8List imageBytes = await compressImage(picture);
Navigator.push(
context,
MaterialPageRoute(
builder: (context) => PreviewPage(
picture: picture,
imageBytes: imageBytes,
cameras: widget.cameras,
),
),
);
} on CameraException catch (e) {
debugPrint('Error occured while taking picture: $e');
return null;
}
}

Future<Uint8List> compressImage(XFile image) async {
final bytes = await image.readAsBytes();
final result = await FlutterImageCompress.compressWithList(
bytes,
minHeight: 1920,
minWidth: 1080,
quality: 80,
rotate: 0,
);
return result;
}

Future initCamera(CameraDescription cameraDescription) async {
_cameraController = CameraController(cameraDescription, ResolutionPreset.high);
try {
await _cameraController.initialize();
if (mounted) {
  setState(() {});
}
} on CameraException catch (e) {
debugPrint("camera error $e");
}
}

@override
Widget build(BuildContext context) {
return Scaffold(
body: SafeArea(
child: Stack(
children: [
(_cameraController.value.isInitialized)
? CameraPreview(_cameraController)
: Container(
color: Colors.black,
child: const Center(child: CircularProgressIndicator()),
),
Align(
alignment: Alignment.bottomCenter,
child: Container(
height: MediaQuery.of(context).size.height * 0.20,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.vertical(top: Radius.circular(24)),
                  color: Colors.black),
              child:
                  Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
                Expanded(
                    child: IconButton(
                  padding: EdgeInsets.zero,
                  iconSize: 30,
                  icon: Icon(
                      _isRearCameraSelected
                          ? CupertinoIcons.switch_camera
                          : CupertinoIcons.switch_camera_solid,
                      color: Colors.white),
                  onPressed: () {
                    setState(
                        () => _isRearCameraSelected = !_isRearCameraSelected);
                    initCamera(widget.cameras[_isRearCameraSelected ? 0 : 1]);
                  },
                )),
                Flexible(
                  flex: 1,
                  child: IconButton(
                  onPressed: takePicture,
                  iconSize: 50,
                  padding: EdgeInsets.zero,
                  constraints: const BoxConstraints(),
                  icon: const Icon(Icons.circle, color: Colors.white),
                ),
                ),

                const Spacer(),
              ]),
            )),
      ]),
    ));
  }
}