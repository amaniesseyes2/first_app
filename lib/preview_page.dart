/*import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class PreviewPage extends StatelessWidget {
  const PreviewPage({
    Key? key,
    required this.picture,
    required this.imageBytes,
    required this.cameras,
  }) : super(key: key);

  final XFile picture;
  final Uint8List imageBytes;
  final List<CameraDescription> cameras;

  // Fonction pour gérer l'enregistrement de l'image dans la galerie
  Future<void> _saveImage(BuildContext context) async {
    final image = File(picture.path);
    final result = await ImageGallerySaver.saveImage(
      await image.readAsBytes(),
    );
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Image enregistrée dans la galerie')),
    );
  }

  // Fonction pour gérer l'envoi de l'image au serveur
  Future<void> _sendImage(BuildContext context) async {
    final pickedFile = File(picture.path);
    final request = http.MultipartRequest(
      'POST',
      Uri.parse('http://127.0.0.1:5000/upload_image'),
    );
    request.files.add(
      await http.MultipartFile.fromPath(
        'image',
        pickedFile.path,
        contentType: MediaType('image', 'jpg'),
      ),
    );
    // Do something with the response
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Image envoyée avec succès !')),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Image.memory(imageBytes),
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed: () => _saveImage(context),
              child: const Text('Enregistrer'),
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed: () => _sendImage(context),
              child: const Text('Envoyer'),
            ),
          ],
        ),
      ),
    );
  }
}
*/
import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:permission_handler/permission_handler.dart';

class PreviewPage extends StatefulWidget {
  const PreviewPage({
    Key? key,
    required this.picture,
    required this.imageBytes,
    required this.cameras,
  }) : super(key: key);

  final XFile picture;
  final Uint8List imageBytes;
  final List<CameraDescription> cameras;

  @override
  _PreviewPageState createState() => _PreviewPageState();
}

class _PreviewPageState extends State<PreviewPage> {
  int id = 0;

  Future<void> _saveImage(BuildContext context) async {
    try {
      final permissionStatus = await Permission.storage.status;
      if (permissionStatus.isDenied) {
        final result = await Permission.storage.request();
        if (result.isDenied) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('L\'autorisation de stockage externe est nécessaire pour enregistrer l\'image.'),
            ),
          );
          return;
        }
      }

      final image = File(widget.picture.path);
      final result = await ImageGallerySaver.saveImage(
        await image.readAsBytes(),
      );
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Image enregistrée dans la galerie')),
      );
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Une erreur est survenue lors de l\'enregistrement de l\'image.')),
      );
    }
  }

  Future<void> _sendImage(BuildContext context, int id) async {
    try {
      final permissionStatus = await Permission.camera.status;
      if (permissionStatus.isDenied) {
        final result = await Permission.camera.request();
        if (result.isDenied) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('L\'autorisation de la caméra est nécessaire pour envoyer l\'image.'),
            ),
          );
          return;
        }
      }

      final pickedFile = File(widget.picture.path);
      final request = http.MultipartRequest(
        'POST',
        Uri.parse('http://192.168.2.181:5000/upload_image'),
      );
      
      request.fields['id'] = (id++).toString();
      request.files.add(
        await http.MultipartFile.fromPath(
          'image',
          pickedFile.path,
          contentType: MediaType('image', 'jpg'),
        ),
      );
      final response = await request.send();
      if (response.statusCode == 200) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Image envoyée avec succès !')),
        );
      } else {
        String errorMsg = 'Une erreur est survenue lors de l\'envoi de l\'image.';
        if (response.statusCode == null) {
          errorMsg += ' Le serveur n\'a pas renvoyé de réponse.';
        } else {
          errorMsg += ' Réponse du serveur : ${response.statusCode}';
        }
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(errorMsg)),
        );
      }
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Une erreur est survenue lors de l\'envoi de l\'image.')),
      );
    }
  }

 @override
Widget build(BuildContext context) {
return Scaffold(
body: Center(
child: Column(
mainAxisAlignment: MainAxisAlignment.center,
children: [
Expanded(
child: Image.memory(widget.imageBytes),
),
const SizedBox(height: 10),
ElevatedButton(
onPressed: () => _saveImage(context),
child: const Text('Enregistrer'),
),
const SizedBox(height: 10),

ElevatedButton(
  onPressed: () => _sendImage(context, id),
child: const Text('Envoyer'),
),
],
),
),
);
}
}
